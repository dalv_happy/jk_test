import React, {Component} from 'react';
import {connect} from 'react-redux';
import { css } from 'emotion'

const listOrganizations = css`
  color: hotpink;
`;

class App extends Component {
    render() {
        console.log(this.props.organizations);
        return (
            <div>
                <ul className={listOrganizations}>
                    {this.props.organizations.map((value, index) => {
                        return <li key={index}>{value}</li>
                    })}
                </ul>
                <input placeholder="ИНН"/>
            </div>
        );
    }
}

export default connect(
    state => ({
        organizations: state.organizations
    }),
    dispatch => ({
        onGetOrganization: (name) => {
            console.log('name', name);
            dispatch({type: 'FIND_TRACK', payload: name});
        },
    })
)(App);
