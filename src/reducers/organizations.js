const initialState = ['Газпром'];

export default function organizations(state = initialState, action) {
    if (action.type === 'FIND_ORGANIZATION') {
        return action.payload;
    }

    return state;
}